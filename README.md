#EVOLUTION RULES OF DETERMINISTIC CELLULAR AUTOMATA FOR MULTICHANNEL SEGMENTATION OF BRAIN TUMORS IN MRI

CUDA kernels + Matlab invocations scripts. 

Using this code for scientific publications requires attribution and citation of the document:

*EVOLUTION RULES OF DETERMINISTIC CELLULAR AUTOMATA FOR MULTICHANNEL SEGMENTATION OF BRAIN TUMORS IN MRI*

*Antonio Rueda Toicen, *
*Rhadamés Carmona, *
*Miguel Martín Landrove, *
*Wuilian Torres*

*Proceedings of the CIMENICS 2014, SVMNI, pages PI25-PI30, ISBN 978-980-7161-04-6, 2014*

Curated test datasets can be found [here](http://bit.ly/1ns1g8k)

Full results of tests can be found [here](http://bit.ly/1vddjyc)

Code is under the [GNU LGPL v3 license](http://www.gnu.org/licenses/lgpl.html)

###Corresponding author, codebase maintainer, and copyright owner:
-*Antonio Rueda Toicen*

 antonio.rueda.toicen [at]  gmail dot com